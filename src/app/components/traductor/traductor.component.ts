import { Component, OnInit } from '@angular/core';
import { TraductorService } from 'src/app/services/traductor.service';

@Component({
  selector: 'app-traductor',
  templateUrl: './traductor.component.html',
  styleUrls: ['./traductor.component.css']
})
export class TraductorComponent implements OnInit {

  lenguajeOrigen:string;
  lenguajeDestino:string;
  palabra:string;
  traduccion:string = "";

  constructor(private serviceTraductor: TraductorService) { }

  cargarOrigen(valor:string):void{
    this.lenguajeOrigen = valor;
  }
  cargarDestino(valor:string):void{
    this.lenguajeDestino = valor;
  }
  traducir(){
    console.log(this.lenguajeOrigen,this.lenguajeDestino,this.palabra);
    this.serviceTraductor.getTranslate(this.lenguajeOrigen,this.lenguajeDestino,this.palabra).subscribe(
      (result)=>{
        console.log(result.outputs[0].output);
        this.traduccion = result.outputs[0].output
      },
      (error)=>{console.log('Ha ocurrido un error!!!')}
    );
  }

  ngOnInit(): void {
  }

}
