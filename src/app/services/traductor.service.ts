import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  constructor(private _http: HttpClient) { }

  public getTranslate(lengOrigen:string,lengDestino:string,palabra:string) : Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        "x-rapidapi-key" : "6af3d92428mshc37bc7e400149b9p18bf2bjsn02435220a6dc",
        "x-rapidapi-host" : "systran-systran-platform-for-language-processing-v1.p.rapidapi.com"
      }),
      params: {
        "source" :lengOrigen,
        "target" :lengDestino,
        "input" : palabra
      }
    };
    return this._http.get("https://systran-systran-platform-for-language-processing-v1.p.rapidapi.com/translation/text/translate",httpOptions);
  }
}
