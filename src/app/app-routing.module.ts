import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CotizacionComponent } from './components/cotizacion/cotizacion.component';
import { EstadisticaComponent } from './components/estadistica/estadistica.component';
import { AboutComponent } from './components/layout/about/about.component';
import { HomeComponent } from './components/layout/home/home.component';
import { TraductorComponent } from './components/traductor/traductor.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'cotizacion', component: CotizacionComponent},
  {path: 'traductor', component: TraductorComponent},
  {path: 'estadistica', component: EstadisticaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
